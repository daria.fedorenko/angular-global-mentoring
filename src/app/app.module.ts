import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { HeaderComponent, FooterComponent } from './core';
import { FakeLogoComponent, BreadcrumbsComponent } from './shared';
import { CoursesComponent } from './modules/courses/pages';
import { CourseItemComponent } from './modules/courses/components';

@NgModule({
  declarations: [
    AppComponent,
    FakeLogoComponent,
    BreadcrumbsComponent,
    HeaderComponent,
    FooterComponent,
    CoursesComponent,
    CourseItemComponent,
  ],
  imports: [BrowserModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
