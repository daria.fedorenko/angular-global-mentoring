import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CoursesComponent } from './courses.component';
import { CourseItemComponent } from '../../components';
import { By } from '@angular/platform-browser';

describe('CoursesComponent', () => {
  let component: CoursesComponent;
  let fixture: ComponentFixture<CoursesComponent>;
  let de: DebugElement;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [CoursesComponent, CourseItemComponent],
      imports: [FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should output input value to console', () => {
    const hostElement = fixture.nativeElement;
    const nameInput: HTMLInputElement = hostElement.querySelector('input');
    const nameButton: HTMLButtonElement = hostElement.querySelector(
      '.courses-search-box__search'
    );

    console.log = jasmine.createSpy('log');

    nameInput.value = 'Hello, world!';
    nameInput.dispatchEvent(new Event('input'));

    nameButton.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(console.log).toHaveBeenCalledWith('Hello, world!');
  });

  it('should ouput child info', () => {
    fixture.detectChanges();
    const childEl: CourseItemComponent = de.query(
      By.directive(CourseItemComponent)
    ).componentInstance;

    console.log = jasmine.createSpy('log');

    childEl.deleteCourseEvent.emit(1);

    expect(console.log).toHaveBeenCalledWith(1);
  });

  it('should output correct value to console on button click', () => {
    const hostElement = fixture.nativeElement;
    const nameButton: HTMLButtonElement = hostElement.querySelector(
      '.courses-more__button'
    );

    console.log = jasmine.createSpy('log');

    nameButton.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(console.log).toHaveBeenCalledWith('Add more courses!');
  });
});
