import { Component, OnInit } from '@angular/core';

import { Course } from 'src/app/shared';

@Component({
  selector: 'ng-pro-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit {
  public nameControl: string;
  public courseList: Course[];

  ngOnInit(): void {
    this.courseList = [
      {
        id: 1,
        title: 'Video Course 1. First course',
        date: '09/16/2020',
        duration: '1h 28min',
        description:
          'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college`s classes. They`re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.',
      },
      {
        id: 2,
        title: 'Video Course 2. Second course',
        date: '10/20/2020',
        duration: '1h 5min',
        description:
          'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college`s classes. They`re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.',
      },
      {
        id: 3,
        title: 'Video Course 3. Third course',
        date: '01/03/2020',
        duration: '1h 18min',
        description:
          'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college`s classes. They`re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.',
      },
      {
        id: 4,
        title: 'Video Course 4. Fourth course',
        date: '06/17/2019',
        duration: '3h 56min',
        description:
          'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college`s classes. They`re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.',
      },
      {
        id: 5,
        title: 'Video Course 5. Fifth course',
        date: '12/27/2019',
        duration: '5h 14min',
        description:
          'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college`s classes. They`re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.',
      },
    ];
  }

  public onSearchOutput(): void {
    console.log(this.nameControl);
  }

  public onDeleteItem(e: any): void {
    console.log(e);
  }

  public onLoadMoreCourses(): void {
    console.log('Add more courses!');
  }

  public trackByFn(index: number, item: Course): number {
    return item.id;
  }
}
