import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Course } from 'src/app/shared';

@Component({
  selector: 'ng-pro-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
})
export class CourseItemComponent {
  @Input()
  course: Course;

  @Output()
  deleteCourseEvent = new EventEmitter<number>();

  public deleteCourse(id: number): void {
    this.deleteCourseEvent.emit(id);
  }
}
