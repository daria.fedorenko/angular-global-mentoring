import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CourseItemComponent } from './course-item.component';

describe('CourseItemComponent', () => {
  let component: CourseItemComponent;
  let fixture: ComponentFixture<CourseItemComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CourseItemComponent],
      schemas: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('.course-item__delete'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger event', () => {
    let deletedId: number;

    component.deleteCourseEvent.subscribe((id: number) => {
      deletedId = id;
    });

    component.deleteCourse(1);

    de.triggerEventHandler('deleteCourse', null);

    expect(deletedId).toBe(1);
  });
});
