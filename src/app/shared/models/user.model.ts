export interface User {
  id: number;
  firstName: string;
  lastName: string;
}

export class Admin implements User {
  id: number;
  firstName: string;
  lastName: string;

  constructor(params: User) {
    this.id = params.id;
    this.firstName = params.firstName;
    this.lastName = params.lastName;
  }
}
