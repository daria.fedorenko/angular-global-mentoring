export interface Course {
  id: number;
  title: string;
  date: string;
  duration: string;
  description: string;
}

export class MyCourse implements Course {
  id: number;
  title: string;
  date: string;
  duration: string;
  description: string;

  constructor(params: Course) {
    this.id = params.id;
    this.title = params.title;
    this.date = params.date;
    this.duration = params.duration;
    this.description = params.description;
  }
}
