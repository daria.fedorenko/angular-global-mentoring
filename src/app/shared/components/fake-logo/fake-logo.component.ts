import { Component } from '@angular/core';

@Component({
  selector: 'ng-pro-fake-logo',
  templateUrl: './fake-logo.component.html',
  styleUrls: ['./fake-logo.component.scss'],
})
export class FakeLogoComponent {}
