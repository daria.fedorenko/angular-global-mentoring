import { Component } from '@angular/core';

@Component({
  selector: 'ng-pro-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {}
